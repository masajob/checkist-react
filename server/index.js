import express from 'express';
import * as db from './utils';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();

db.setUpConnection();

app.use(bodyParser.json());
app.use(cors({ origin: '*' }));

app.listen(3000, () => {
  console.log('Сервер запущен на localhost:3000');
});

/* ROUTING modules */

app.get('/api/modules', (req,res) => {
  db.listModules().then(data => res.json(data));
});
app.post('/api/modules', (req,res) => {
  db.createModule(req.body).then(data => res.json(data));
});

/* ROUTING presets */

app.get('/api/presets', (req,res) => {
  db.listPresets().then(data => res.json(data));
});
app.post('/api/presets', (req,res) => {
  db.createPreset(req.body).then(data => res.json(data));
});

/* ROUTING submodules */

app.get('/api/submodules', (req,res) => {
  db.listSubmodules().then(data => res.json(data));
});
app.post('/api/submodules', (req,res) => {
  db.createSubmodule(req.body).then(data => res.json(data));
});

/* ROUTING workers */

app.get('/api/workers', (req,res) => {
  db.listWorkers().then(data => res.json(data));
});
app.post('/api/workers', (req,res) => {
  db.createWorkers(req.body).then(data => res.json(data));
});

/* ROUTING organization */

app.get('/api/organizations', (req,res) => {
  db.listOrganizations().then(data => res.json(data));
});
app.post('/api/organizations', (req,res) => {
  db.createOrganizations(req.body).then(data => res.json(data));
});