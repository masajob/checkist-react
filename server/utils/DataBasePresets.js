import mongoose from 'mongoose';
import '../models/Presets';

const Presets = mongoose.model('Presets');

export function listPresets() {
  return Presets.find();
}

export function createPreset(data) {
  if (data) {
    const presets = new Presets ({
      name: data.name,
      selected: data.selected,
      internalModulesId: data.internalModulesId
    });

    return presets.save();
  }

  return Presets.find();
}