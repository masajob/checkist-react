import mongoose from 'mongoose';
import '../models/Modules';

const Modules = mongoose.model('Modules');

export function listModules() {
  return Modules.find();
}

export function createModule(data) {
  if (data) {
    const modules = new Modules ({
      name: data.name,
      selected: data.selected,
      internalSubModuleItems: data.internalSubModuleItems
    });

    return modules.save();
  }

  return Modules.find();
}