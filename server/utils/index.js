import mongoose from 'mongoose';
import config from '../config/config.json';

export * from './DataBaseModules';
export * from './DataBasePresets';
export * from './DataBaseSubmodules';
export * from './DataBaseWorkers';
export * from './DataBaseOrganization';

export function setUpConnection () {
  mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.name}`);
}