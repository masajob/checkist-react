import mongoose from 'mongoose';
import '../models/Organization';

const Organization = mongoose.model('Organization');

export function listOrganizations() {
  return Organization.find();
}

export function createOrganizations(data) {
  if (data) {
    const organization = new Organization ({
      name: data.name
    });

    return organization.save();
  }

  return Organization.find();
}