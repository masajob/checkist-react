import mongoose from 'mongoose';
import '../models/Workers';

const Workers = mongoose.model('Workers');

export function listWorkers() {
  return Workers.find();
}

export function createWorkers(data) {
  if (data) {
    const workers = new Workers ({
      position: data.position,
      level: data.level,
      levelText: data.levelText,
      costPerHour: data.costPerHour,
      orgAffiliation: data.orgAffiliation
    });

    return workers.save();
  }

  return Workers.find();
}