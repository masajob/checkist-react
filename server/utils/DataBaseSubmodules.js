import mongoose from 'mongoose';
import '../models/Submodules';

const Submodules = mongoose.model('Submodules');

export function listSubmodules() {
  return Submodules.find();
}

export function createSubmodule(data) {
  if (data) {
    const submodules = new Submodules ({
      name: data.name,
      description: data.description,
      selected: data.selected,
      selectedDefault: data.selectedDefault,
      frontHours: data.frontHours,
      backendHours: data.backendHours,
      designerHours: data.designerHours
    });

    return submodules.save();
  }

  return Submodules.find();
}