const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const WorkersSchema = new Schema({
  position: { type: Number, required: true},
  level: { type: Number, required: true},
  levelText: { type: String, required: true},
  costPerHour: { type: Number, required: true},
  orgAffiliation: { type: String, required: true}
});

mongoose.model('Workers', WorkersSchema);