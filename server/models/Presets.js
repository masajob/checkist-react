const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PresetsSchema = new Schema({
  name: { type: String, required: true},
  selected: { type: Boolean, required: true},
  internalModulesId: { type: Array, required: true}
});

mongoose.model('Presets', PresetsSchema);