const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ModulesSchema = new Schema({
    name: { type: String, required: true},
    selected: { type: Boolean, required: true},
    internalSubModuleItems: { type: Array, required: true}
});

mongoose.model('Modules', ModulesSchema);