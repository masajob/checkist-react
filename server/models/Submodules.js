const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const SubmodulesSchema = new Schema({
  name: { type: String, required: true},
  description: { type: Schema.Types.Mixed, required: true},
  selected: { type: Boolean, required: true},
  selectedDefault: { type: Boolean, required: true},
  frontHours: { type: Number, required: true},
  backendHours: { type: Number, required: true},
  designerHours: { type: Number, required: true}
});

mongoose.model('Submodules', SubmodulesSchema);