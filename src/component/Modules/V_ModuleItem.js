import React, { Component } from 'react'

class V_ModuleItem extends Component {

    render(){

        let { _id, name, selected, internalSubModuleItems } = this.props.static;
        let { toggleModule } = this.props.logic;

        return (
            <div className = { selected ? 'module-item selected':'module-item' }
                 onClick = { () => toggleModule( _id, internalSubModuleItems ) }
            >
                { name }
            </div>
        );
    }
}

export default V_ModuleItem;
