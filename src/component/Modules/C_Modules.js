import { connect } from 'react-redux'
import L_Modules from './L_Modules'
import { aToggleModule,
         aClearPresets,
         aGetVariants,
         aSelectDefaultParagraph } from '../../action'

let fullState = {}

const mapStateToProps = (state) => {
  fullState = state;
  return { modules: state.modules.data }
}

const mapDispatchToProps = (dispatch) => ({
    toggleModule: (id, arrId) => {
      dispatch(aToggleModule(id))
      dispatch(aClearPresets())
      dispatch(aSelectDefaultParagraph(arrId))
      dispatch(aGetVariants(fullState))
    }
})

const C_Modules = connect (
    mapStateToProps,
    mapDispatchToProps
)(L_Modules)

export default C_Modules