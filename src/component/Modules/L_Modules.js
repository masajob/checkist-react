import React, { Component } from 'react';
import V_ModuleItem from './V_ModuleItem';
import './style.css';

class L_Modules extends Component {

    render(){

        let modules = this.props.modules || [];

        return (
            <div className="col-12 col-md-4">
                <div className='container-modules'>

                  {
                    modules.map( (item) => {
                       return <V_ModuleItem key = {item._id} static = { item } logic = { this.props } />
                    })
                  }

                </div>
            </div>
        )
    }
}

export default L_Modules
