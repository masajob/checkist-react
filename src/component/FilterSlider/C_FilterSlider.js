import { connect } from 'react-redux'
import V_FilterSlider from './V_FilterSlider'
import {
    aFilterVariants,
    aChangeVisibleVariants,
    aRemoveHiddenVariants
} from '../../action'

const mapStateToProps = (state) => ({
    dataSlider: state.calculations
})

const mapDispatchToProps = (dispatch) => ({
    changeSliderValue : (val) => {
        dispatch(aFilterVariants(val))
        dispatch(aChangeVisibleVariants(val))
        dispatch(aRemoveHiddenVariants())
    }
})

const C_FilterSlider = connect(
    mapStateToProps,
    mapDispatchToProps
)(V_FilterSlider)

export default C_FilterSlider
