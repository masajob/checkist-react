import React, { Component } from 'react'
import InputRange from 'react-input-range'
import 'react-input-range/lib/css/index.css'
import './style.css'

class V_FilterSlider extends Component {

    constructor(){
        super()
        this.state = {
            valueSlider: {
                min: 0,
                max: 0
            }
        }
    }

    componentWillMount = () => {
        this.setState({
          valueSlider: {
            min: +this.props.dataSlider.minSum,
            max: +this.props.dataSlider.maxSum
          }
        })
    }

    dragStop = (val) => {
        this.props.changeSliderValue(val)
    }


    render(){

        if(!this.props.dataSlider.minSum) return null;

        return (

            <div className="filter-checklist">
                <div className="slider-wrapper">
                    <div className="row">

                        <div className="col">
                            <div className="checklist-slider-wrapper">
                                <InputRange
                                    draggableTrack
                                    maxValue={this.props.dataSlider.maxSum}
                                    minValue={this.props.dataSlider.minSum}
                                    value={this.state.valueSlider}
                                    onChange={value =>  this.setState({ valueSlider: value }) }
                                    onChangeComplete={value => this.dragStop(value)}
                                />
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        )
    }
}
export default V_FilterSlider
