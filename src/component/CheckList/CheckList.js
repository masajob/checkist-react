import React, { Component } from 'react'
import Button from 'material-ui/Button'
import { Link } from 'react-router-dom'
import CFilterSlider from '../FilterSlider/C_FilterSlider'
import CListVariants from '../ListVariants/C_ListVariants'
import './style.css'

class CheckList extends Component {
    render(){
        return (
            <div className="container">

              <div className="button-wrapper">

                <div className="row">

                  <div className="col-md-auto">
                    <Link to="/">
                      <div className="button-back-wrapper">
                        <Button variant="raised" color="primary">
                          Назад к выбору исполнителя
                        </Button>
                      </div>
                    </Link>
                  </div>

                  <div className="col">
                    <div className="button-next-wrapper">
                      <Button variant="raised" color="primary">
                        Отправить бриф
                      </Button>
                    </div>
                  </div>

                </div>

              </div>

              <CFilterSlider />

              <CListVariants/>

            </div>
        )
    }
}
export default CheckList