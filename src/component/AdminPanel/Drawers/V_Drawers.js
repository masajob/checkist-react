import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import List, { ListItem, ListItemText } from 'material-ui/List';
import V_TableData from '../TableData/V_Table';
import Button from 'material-ui/Button';
import { Route }     from 'react-router';
import CreateItem from '../CreateItem/V_CreateItem';
import { Link } from 'react-router-dom';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    flex: 1
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    minWidth: 0, // So the Typography noWrap works
    display: 'flex',
    flexDirection: 'column'
  },
  toolbar: theme.mixins.toolbar,
  chip: {
    margin: theme.spacing.unit,
  },
  mainHeader: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  mainTitle: {
    fontSize: '30px'
  },
  btn: {
    backgroundColor: '#3f51b5',
    color: 'white',
    '&:hover': {
      backgroundColor: '#5fc000',
    }
  }
});

class ClippedDrawer extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      data: []
    }
  }

  togglePanel = (item) => {
    this.setState({ data: item })
  }

  render(){

    const { classes, data, createItem } = this.props

    return (
        <div className={classes.root}>
          <Drawer
              variant="permanent"
              classes={{
                paper: classes.drawerPaper,
              }}
          >
            <div className={classes.toolbar} />

            {

              data.map((item, index) => {
                return (
                    <List key={index}>
                      <Link to={'/admin/' + item.name } >
                        <ListItem button onClick={ () => this.togglePanel(item) }>
                          <ListItemText primary={ item.nameRus } />
                        </ListItem>
                      </Link>
                    </List>
                )
              })

            }

          </Drawer>
          <div>
            <Route path="/admin/modules"  component={CreateItem} />
          </div>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <div className={classes.mainHeader}>
              <div className={classes.mainTitle}>
                {this.state.data.nameRus}
              </div>
              <Link to="/checklist" >
                {
                  this.state.data.length === 0 ?
                      '' :
                      <Button className={classes.btn}
                              onClick={ () => createItem(this.state.data.name) }
                      >
                        Создать
                      </Button>
                }
              </Link>
            </div>
            <V_TableData stat={this.state.data} logic={this.props}/>
          </main>
        </div>
    )
  }
}

ClippedDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ClippedDrawer);