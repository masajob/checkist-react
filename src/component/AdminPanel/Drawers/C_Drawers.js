import { connect } from 'react-redux'
import V_Drawers from './V_Drawers'

let showData = [
  {
    name: 'presets',
    nameRus: 'Пресеты',
    showColumn: [
      {
        colName: '_id',
        colNameRus: 'ID'
      }, {
        colName: 'name',
        colNameRus: 'Название пресета'
      }
    ]
  }, {
    name: 'modules',
    nameRus: 'Модули',
    showColumn: [
      {
        colName: '_id',
        colNameRus: 'ID'
      }, {
        colName: 'name',
        colNameRus: 'Название модуля'
      }
    ]
  }, {
    name: 'submodules',
    nameRus: 'Подмодули',
    showColumn: [
      {
        colName: '_id',
        colNameRus: 'ID'
      }, {
        colName: 'name',
        colNameRus: 'Название подмодуля'
      }, {
        colName: 'backendHours',
        colNameRus: 'back'
      }, {
        colName: 'frontHours',
        colNameRus: 'front'
      }, {
        colName: 'designerHours',
        colNameRus: 'des'
      }
    ]
  }, {
    name: 'organizations',
    nameRus: 'Организации',
    showColumn: [
      {
        colName: '_id',
        colNameRus: 'ID'
      }, {
        colName: 'name',
        colNameRus: 'Название организации'
      }
    ]
  }, {
    name: 'workers',
    nameRus: 'Сотрудники',
    showColumn: [
      {
        colName: '_id',
        colNameRus: 'ID'
      }, {
        colName: 'levelText',
        colNameRus: 'Уровень'
      }, {
        colName: 'position',
        colNameRus: 'Позиция'
      }, {
        colName: 'costPerHour',
        colNameRus: 'Стоимость, час'
      }
    ]
  }
]

function filterState (state, showData) {

  let filterData = []

  for( let item in state ) {
    showData.map((show) => {
      if (show.name === item) {
        filterData.push({
          name: show.name,
          nameRus: show.nameRus,
          showColumn: show.showColumn,
          data: state[item][item]
        })
      }
    })
  }

  return filterData
}

const mapStateToProps = (state) => ({
  data: filterState(state, showData)
})

const mapDispatchToProps = (state) => ({
  createItem : (requestName) => {
    console.log(requestName)
  }
})

const C_Drawers = connect(
    mapStateToProps,
    mapDispatchToProps
)(V_Drawers)

export default C_Drawers