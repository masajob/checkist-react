import React, { Component } from 'react'
import AdminBar from './AdminBar/V_AdminBar'
import C_LeftPanel from './LeftPanel/C_LeftPanel'
import C_RightPanel from './RightPanel/C_RightPanel'
import { withStyles } from 'material-ui/styles'
import styles from './style'

class AdminPanel extends Component {

  render(){
      return (
          <div style={styles.adminPanel}>

            <AdminBar />

            <main style={styles.root}>
              <C_LeftPanel />
              {/*<C_RightPanel />*/}
            </main>

          </div>
      )
  }
}

export default withStyles(styles)(AdminPanel);