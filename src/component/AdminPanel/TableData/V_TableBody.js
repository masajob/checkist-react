import React from 'react'
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import styles from './style'
import { withStyles } from 'material-ui/styles'
import { TableBody, TableCell, TableRow } from 'material-ui/Table'
import Delete from '@material-ui/icons/Delete'
import Create from '@material-ui/icons/Create'

class V_TableData extends React.Component {

  render() {

    const { classes } = this.props
    const { showColumn, data, name } = this.props.stat
    const { createItem, deleteItem } = this.props.logic

    if(!showColumn || !data) return null

    return (
      <TableBody>

        {
          data.map((item) => {

            return (
                <TableRow key={ item._id }>

                  {
                    showColumn.map((show) => {
                      if(show.colName in item)
                        return (
                            <TableCell key={ show.colName + item._id } >
                              { item[show.colName] }
                            </TableCell>
                        )
                    })
                  }

                  <TableCell>
                    <Link to={`/admin/update/${name}/${item._id}`} >
                      <Create color="primary" className={ classes.icon } />
                    </Link>
                  </TableCell>

                  <TableCell>
                    <Delete color="primary"
                            className={ classes.icon }
                            onClick={ () => console.log(item._id) } />
                  </TableCell>

                </TableRow>
            );

          })
        }

      </TableBody>
    );
  }

}

V_TableData.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(V_TableData)