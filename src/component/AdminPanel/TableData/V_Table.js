import React from 'react';
import PropTypes from 'prop-types';
import styles from './style';
import { withStyles } from 'material-ui/styles';
import Table from 'material-ui/Table';
import Paper from 'material-ui/Paper';

import V_TableHead from './V_TableHead'
import V_TableBody from './V_TableBody'

class V_Table extends React.Component {

  render() {

    const { classes, stat, logic } = this.props;

    return (
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <V_TableHead stat={stat} />
            <V_TableBody stat={stat} logic={logic} />
          </Table>
        </Paper>
    );
  }

}

V_Table.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(V_Table);