const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700,
  },
  icon: {
    cursor: 'pointer',
    transition: '.3s',
    '&:hover': {
      color: '#5fc000',
    }
  }
});

export default styles;