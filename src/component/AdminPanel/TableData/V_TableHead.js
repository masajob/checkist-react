import React from 'react'
import PropTypes from 'prop-types'
import { TableCell, TableHead, TableRow } from 'material-ui/Table'

class V_TableHead extends React.Component {

  render() {

    const { showColumn } = this.props.stat

    if(!showColumn) return null

    return (
        <TableHead>
          <TableRow>
            {
              showColumn.map((item,index) => {
                return <TableCell key={item.colName + '_' + index}>{item.colNameRus}</TableCell>
              })
            }
            <TableCell></TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
    )
  }
}

V_TableHead.propTypes = {
  showColumn: PropTypes.array.isRequired,
};

export default V_TableHead