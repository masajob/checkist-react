import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from 'material-ui/styles'
import List, { ListItem, ListItemText } from 'material-ui/List'
import { Link } from 'react-router-dom'
import styles from './style'

class V_LeftPanel extends Component {

  render() {

    let { data, route, changeRoute } = this.props

    return (

        <div style={styles.leftPanel}>
          <List>
            {
              data.map((item, index) => {
                return (
                  <Link to={'/admin/' + item.name }
                        key={index}
                        onClick = { () => changeRoute(item.name) }
                  >
                    <ListItem button
                              style = { item.name === route ? styles.active : {} }
                    >
                      <ListItemText primary={ item.nameRus } />
                    </ListItem>
                  </Link>
                )
              })
            }
          </List>
        </div>

    )
  }
}

V_LeftPanel.propTypes = {
  data: PropTypes.array.isRequired,
  route: PropTypes.string.isRequired,
  changeRoute: PropTypes.func.isRequired
}

export default withStyles(styles)(V_LeftPanel)