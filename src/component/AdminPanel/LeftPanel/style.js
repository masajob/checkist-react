const styles = {
  leftPanel: {
    borderRight: '1px solid lightgray',
    width: 240,
    paddingTop: 20
  },
  active: {
    backgroundColor: 'lightgray'
  },
}

export default styles