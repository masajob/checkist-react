import { connect }           from 'react-redux'
import V_LeftPanel           from './V_LeftPanel'
import filterState           from '../setings'
import { aChangeAdminRoute } from '../../../action/actionAdminRoute'

const mapStateToProps = (state) => ({
  data: filterState(state),
  route: state.adminRoute.route
})

const mapDispatchToProps = (dispatch) => ({
  changeRoute : (route) => {
    dispatch(aChangeAdminRoute(route))
  }
})

const C_Drawers = connect(
    mapStateToProps,
    mapDispatchToProps
)(V_LeftPanel)

export default C_Drawers