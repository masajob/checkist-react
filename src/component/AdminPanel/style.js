const styles = {
  adminPanel: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh'
  },
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    flex: 1
  }
};

export default styles