import { connect } from 'react-redux';
import L_Submodule from './L_Submodules';
import { aToggleParagraph,
         aSelectAllParagraph,
         aSelectDefaultParagraph,
         aToggleModule,
         aGetVariants,
         aClearPresets } from '../../action';
import './style.css';

let fullState = {};

const mapStateToProps = (state) => {
  fullState = state
  return {
    modules: state.modules.data,
    submodules: state.submodules.data
  }
}

const mapDispatchToProps = (dispatch) => ({
  toggleParagraph: (id) => {
    dispatch(aToggleParagraph(id));
    dispatch(aGetVariants(fullState));
  },
  selectAllParagraph: (arrId) => {
    dispatch(aSelectAllParagraph(arrId));
    dispatch(aGetVariants(fullState));
  },
  closeSubmodule: (id, arrId) => {
    dispatch(aToggleModule(id));
    dispatch(aClearPresets());
    dispatch(aSelectDefaultParagraph(arrId));
    dispatch(aGetVariants(fullState));
  }
})

const C_Submodules = connect (
    mapStateToProps,
    mapDispatchToProps
)(L_Submodule)

export default C_Submodules


