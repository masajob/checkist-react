import React, { Component } from 'react';

class SubParagraph extends Component {

    render(){

        let { _id, name, selected } = this.props.static;
        let { toggleParagraph } = this.props.logic;

        return (
            <div className={ selected ? 'sub-module-item selected' : 'sub-module-item' }
                onClick={ () => toggleParagraph(_id) }
            >
              { name }
            </div>
        )
    }
}

export default SubParagraph
