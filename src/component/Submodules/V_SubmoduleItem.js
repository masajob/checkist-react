import React, { Component } from 'react'
import V_Subparagraph from './V_Subparagraph'


class V_SubmoduleItem extends Component {

    render() {

        let { _id, name, internalSubModuleItems } = this.props.static;
        let subparagraph = this.props.logic.submodules || [];
        let { selectAllParagraph, closeSubmodule } = this.props.logic;

        return (

            <div className='sub-module'>

                <h2>{ name }</h2>

                <div className = 'close-sub-module'
                     onClick   = { () => closeSubmodule( _id, internalSubModuleItems ) }
                >
                  &times;
                </div>

                <div className='sub-paragraph-wrapper'>

                  {

                    subparagraph.map((item) => {

                      return internalSubModuleItems.map((sub) => {

                        if( sub === item._id ) {
                          return (
                              <V_Subparagraph
                                  key = { item._id }
                                  static = { item }
                                  logic = { this.props.logic }
                              />
                          )
                        }

                      });

                    })
                  }

                </div>

                <div className='footer-sub-module'>

                    <div className='sub-module-select-all'
                         onClick={ () => selectAllParagraph(internalSubModuleItems) }
                    >
                      выбрать все
                    </div>

                </div>

            </div>
        )
    }
}

export default V_SubmoduleItem;
