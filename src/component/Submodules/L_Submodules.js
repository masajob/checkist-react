import React, { Component } from 'react'
import V_SubmoduleItem from './V_SubmoduleItem'

class L_Submodules extends Component {

  render(){

      let modules = this.props.modules || [];

      return (
          <div className="col-12 col-md-8">
              <div className='container-sub-modules'>
                  {
                    modules.map((item) => {

                      if(!item.selected) return null;

                      return (
                          <V_SubmoduleItem
                              key    = { item._id }
                              static = { item }
                              logic  = { this.props }
                          />
                      )
                    })
                  }
              </div>
          </div>
      )
  }
}
export default L_Submodules;
