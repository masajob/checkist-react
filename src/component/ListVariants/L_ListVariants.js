import React, { Component } from 'react'
import Table, {
    TableHead,
    TableBody,
    TableRow,
    TableCell
} from 'material-ui/Table';
import Checkbox from 'material-ui/Checkbox';
import './style.css'

class ListVariants extends Component {

    constructor(){
        super()
        this.countChecked = 0
        this.state = {
            isSelectAll: false
        }
    }

    getElement = (obj) => {
        if(obj === null) return
        switch (obj.level){
            case 1:
                return <div className="cercl-class green-class"></div>
            case 2:
                return <div className="cercl-class yellow-class"></div>
            case 3:
                return <div className="cercl-class red-class"></div>
            default:
                return
        }
    }

    allSelection = () => {

        this.setState({ isSelectAll: !this.state.isSelectAll })

        if(this.state.isSelectAll) {
          this.props.removeAll()
          this.countChecked = 0
        } else {
          this.props.selectedAll()
          this.countChecked = this.props.arrList.length
        }

    }

    oneSelection = (id, checked) => {

        checked ? this.countChecked-- : this.countChecked++

        this.props.selectOne(id)

        if(this.countChecked === this.props.arrList.length) {
            this.setState({ isSelectAll: true })
        } else {
            this.setState({ isSelectAll: false })
        }
    }

    render(){

        if(!this.props.arrList.length) return <div>Нет данных</div>
        const { arrList } = this.props

        return (
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell padding="checkbox">
                                <Checkbox
                                    checked={this.state.isSelectAll}
                                    onChange={this.allSelection}
                                />
                            </TableCell>
                            <TableCell>Организация</TableCell>
                            <TableCell>Стоимость</TableCell>
                            <TableCell>des</TableCell>
                            <TableCell>front</TableCell>
                            <TableCell>back</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                         {
                             arrList.map((item) => {

                                 const isSelected = item.checked;

                                 return (
                                     <TableRow key={item.id} selected={item.checked}>

                                         <TableCell padding="checkbox">
                                             <Checkbox checked={ isSelected }
                                                       onChange={ () => this.oneSelection(item.id, item.checked) }
                                             />
                                         </TableCell>

                                         <TableCell>
                                             {item.organizationName}
                                         </TableCell>

                                         <TableCell>
                                             {item.sum}
                                         </TableCell>

                                         <TableCell>
                                             { this.getElement(item.des) }
                                         </TableCell>

                                         <TableCell>
                                             { this.getElement(item.front) }
                                         </TableCell>

                                         <TableCell>
                                             { this.getElement(item.back) }
                                         </TableCell>

                                     </TableRow>
                                 )
                             })
                         }
                    </TableBody>
                </Table>
        )
    }
}
export default ListVariants