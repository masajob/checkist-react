import { connect } from 'react-redux'
import {
    aSelectOneVariant,
    aSelectAllVariants,
    aRemoveAllVariants
} from '../../action'
import L_ListVariants from './L_ListVariants'

const filterVariants = (state) => {

    let resultOption = state.calculations.resultOption || []

    resultOption.sort((a,b) => {
        if(a.sum > b.sum) return 1
        if(a.sum < b.sum) return -1
        if(a.sum === b.sum){
          if(a._id > b._id) return 1
          if(a._id < b._id) return -1
        }
        return 0
    })

    resultOption = resultOption.filter((item) => {
        return item.visible === true
    })

    return resultOption
}

const mapStateToProps = (state) => ({
    arrList: filterVariants(state)
})

const mapDispatchToProps = (dispatch) => ({
  selectOne: (id) => {
    dispatch(aSelectOneVariant(id))
  },
  selectedAll: () => {
    dispatch(aSelectAllVariants())
  },
  removeAll: () => {
    dispatch(aRemoveAllVariants())
  }
})

const C_ListVariants = connect(
    mapStateToProps,
    mapDispatchToProps
)(L_ListVariants)

export default C_ListVariants