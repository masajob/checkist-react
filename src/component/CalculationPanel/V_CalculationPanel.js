import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Button from 'material-ui/Button'
import './style.css'


class V_CalculationPanel extends Component {

    render(){

        console.log(this.props.data)
        return (
            <div className="calculation-panel">
                <div className="container">
                    <div className="calculation-panel-wrapper">
                        <div className="calculation-panel-sum">
                            <b>
                                Диапазон цен: {this.props.min} - {this.props.max} руб.
                            </b>
                        </div>
                        <Link to="/checklist" >
                            <Button variant="raised" color="primary">
                                Перейти к выбору
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}
export default V_CalculationPanel