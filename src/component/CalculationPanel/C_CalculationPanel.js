import  { connect } from 'react-redux'
import V_CalculationPanel from './V_CalculationPanel'

const mapStateToProps = (state) => ({
    min: state.calculations.minSum,
    max: state.calculations.maxSum,
    data: state
})

const mapDispatchToProps = (state) => ({

})

const C_CalculationPanel = connect(
    mapStateToProps,
    mapDispatchToProps
)(V_CalculationPanel)

export default C_CalculationPanel