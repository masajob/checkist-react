import React, { Component } from 'react'
import V_PresetItem from './V_PresetItem';
import './style.css'

class L_Presets extends Component {

    render(){

      let presets = this.props.presets || [];

      return (
         <div className='module-presets'>
             <div className='container'>
                 <div className="row">
                     <div className="col-12">
                         <div className='wrapper-module-presets'>
                             <h3>Пресеты модулей по типу сайта</h3>
                              <div className='container-presets'>

                                {
                                  presets.map( (item) => {
                                    return <V_PresetItem key = {item._id} static = { item } logic = { this.props } />;
                                  })
                                }

                              </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
      )

    }
}
export default L_Presets;
