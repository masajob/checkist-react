import { connect } from 'react-redux'
import L_Presets from './L_Presets'
import { aTogglePreset,
         aToggleModules,
         aGetVariants,
         aSelectDefaultParagraph } from '../../action'

let fullState = {}

const mapStateToProps = (state) => {
  fullState = state;
  return { presets: state.presets.data }
}

const mapDispatchToProps = (dispatch) => ({
    togglePresets: (id, ModulesId) => {
      dispatch(aTogglePreset(id))
      dispatch(aToggleModules(ModulesId))
      dispatch(aSelectDefaultParagraph())
      dispatch(aGetVariants(fullState))
    }
})

const C_Preset = connect(
    mapStateToProps,
    mapDispatchToProps
)(L_Presets)

export default C_Preset

