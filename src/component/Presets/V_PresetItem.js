import React, { Component } from 'react'

class V_PresetItem extends Component {

    render(){

        let { _id, name, selected, internalModulesId} = this.props.static;
        let { togglePresets } = this.props.logic;

        return (

            <div className={ selected ? 'preset-item selected':'preset-item' }
                 onClick={ () => togglePresets(_id, internalModulesId) }
            >
                 {name}
            </div>

        )
    }
}

export default V_PresetItem