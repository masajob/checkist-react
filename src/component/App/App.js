import React, { Component } from 'react';
import { connect } from 'react-redux';

import C_Presets from '../Presets/C_Presets';
import C_Modules from '../Modules/C_Modules';
import C_Submodules from '../Submodules/C_Submodules';
import CCalculationPanel from '../CalculationPanel/C_CalculationPanel';

import './bootstrapGrid.css';
import './resetStyle.scss';
import './animation.css';

class App extends Component {

    render() {

        let { result } = this.props
        let showPanel = result.length ?  true : false

        return (
            <div>
                <C_Presets />
                <div className='module-modules'>
                    <div className='container'>
                        <div className="row">
                            <C_Modules />
                            <C_Submodules />
                        </div>
                    </div>
                </div>
                {showPanel && <CCalculationPanel />}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    result: state.calculations.resultOption
})

const mapDispatchToProps = (dispatch) => ({

})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)


