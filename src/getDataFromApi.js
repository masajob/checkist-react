import { aGetData } from './action'
import settings from './settings'

export default function getAllData (store) {
  store.dispatch(aGetData(`${settings.prefix}:${settings.port}/${settings.presets}`))
  store.dispatch(aGetData(`${settings.prefix}:${settings.port}/${settings.workers}`))
  store.dispatch(aGetData(`${settings.prefix}:${settings.port}/${settings.modules}`))
  store.dispatch(aGetData(`${settings.prefix}:${settings.port}/${settings.submodules}`))
  store.dispatch(aGetData(`${settings.prefix}:${settings.port}/${settings.organizations}`))
}
