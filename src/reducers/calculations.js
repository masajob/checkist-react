import Calc from '../libs/Calculator';
import {
  GET_VARIANTS ,
  SELECT_ONE_VARIANT,
  SELECT_ALL_VARIANTS,
  REMOVE_ALL_VARIANTS,
  REMOVE_HIDDEN_VARIANTS,
  FILTER_VARIANTS,
  CHANGE_VISIBLE_VARIANTS
} from '../action/actionsCalculations'

const initialState = {
  resultOption: []
}

export function calculations(state = initialState, action) {
  switch (action.type){

    case GET_VARIANTS:
      return Object.assign({}, state, { ... new Calc(action.state).getOptions() })

    case SELECT_ONE_VARIANT:
      return Object.assign({}, state, { resultOption: state.resultOption.map((item) => {
          if(item.id === action.id) item.checked = !item.checked
          return item
        })
      })

    case SELECT_ALL_VARIANTS:
      return Object.assign({}, state, { resultOption: state.resultOption.map((item) => {
          if(item.visible) item.checked = true
          return item
        })
      })

    case REMOVE_ALL_VARIANTS:
      return Object.assign({}, state, { resultOption: state.resultOption.map((item) => {
          item.checked = false
          return item
        })
      })

    case REMOVE_HIDDEN_VARIANTS:
      return Object.assign({}, state, { resultOption: state.resultOption.map((item) => {
          if(!item.visible) item.checked = false
          return item
        })
      })

    case FILTER_VARIANTS:
      return Object.assign({}, state, { sliderValue: {
          min: action.valueInterval.min,
          max: action.valueInterval.max
        }
      })

    case CHANGE_VISIBLE_VARIANTS:
      return Object.assign({}, state, { resultOption: state.resultOption.map((item) => {
          if(item.sum >= action.valueInterval.min && item.sum <= action.valueInterval.max) {
            item.visible = true
          } else {
            item.visible = false
          }
          return item
        })
      })

    default:
      return state

  }
}
