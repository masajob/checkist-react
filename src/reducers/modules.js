import {
  CHANGE_STATUS_LOAD_MODULES,
  RECEIVE_MODULES,
  TOGGLE_MODULE,
  TOGGLE_MODULES
} from '../action/actionsModules'

const initialState = {
  isLoad: false,
  lastAddedModule: 1,
  nameRus: 'Модули'
}

export function  modules (state = initialState, action){

  switch (action.type){

    case CHANGE_STATUS_LOAD_MODULES:
      return Object.assign({}, state, { isLoad: action.bool })

    case RECEIVE_MODULES:
      return Object.assign({}, state, { data: action.data })

    case TOGGLE_MODULE:
      return Object.assign({}, state, { data: state.data.map((item) => {
          if ( item._id === action.id ) {
            item.selected = !item.selected;
          }
          return item;
        })
      })

    case TOGGLE_MODULES:
      return Object.assign({}, state, { data: state.data.map((item) => {
          item.selected = false;
          action.arr.map((sub) => {
            if (item._id === sub) {
              item.selected = true;
            }
          })
          return item;
        })
      })

    default:
      return state

  }

}
