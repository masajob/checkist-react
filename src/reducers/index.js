import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import { modules } from './modules'
import { presets } from './presets'
import { submodules } from './submodules'
import { workers } from './workers'
import { organizations } from './organizations'
import { calculations } from './calculations'
import { adminRoute } from './adminroute'

const checkistApp = combineReducers({
    modules: modules,
    presets: presets,
    submodules: submodules,
    calculations: calculations,
    workers: workers,
    organizations: organizations,
    adminRoute: adminRoute,
    router: routerReducer
})

export default checkistApp