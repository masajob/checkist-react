import {
  CHANGE_STATUS_LOAD_SUBMODULES,
  RECEIVE_SUBMODULES,
  TOGGLE_PARAGRAPH,
  SELECT_DEFAULT_PARAGRAPH,
  SELECT_ALL_PARAGRAPH,
  CLOSE_SUBMODULES
} from '../action/actionsSubmodules';

const initialState = {
  isLoad: false,
  nameRus: 'Подмодули'
}

export function  submodules (state = initialState, action){

  switch (action.type){

    case CHANGE_STATUS_LOAD_SUBMODULES:
      return Object.assign({}, state, { isLoad: action.bool })

    case RECEIVE_SUBMODULES:
      return Object.assign({}, state, { data: action.data })

    case TOGGLE_PARAGRAPH:
      return Object.assign({}, state, { data: state.data.map((item) => {
          if (item._id === action.id) {
            item.selected = !item.selected;
          }
          return item;
        })
      })

    case SELECT_ALL_PARAGRAPH:
      return Object.assign({}, state, { data: state.data.map((item) => {
          action.arrId.map((sub) => {
            if (sub === item._id) {
              item.selected = true;
            }
          })
          return item;
        })
      })

    case SELECT_DEFAULT_PARAGRAPH:
      return Object.assign({}, state, { data: state.data.map((item) => {
          if (action.arrId.length > 0) {
            action.arrId.map((sub) => {
              if(sub === item._id) item.selected = item.selectedDefault;
            })
          } else {
            item.selected = item.selectedDefault
          }
          return item;
        })
      })

    default:
      return state

  }

}