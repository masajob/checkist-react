import {
  CHANGE_STATUS_LOAD_ORGANIZATIONS,
  RECEIVE_ORGANIZATIONS
} from '../action/actionsOrganizations';

const initialState = {
  isLoad: false,
  nameRus: 'Организация'
}

export function  organizations (state = initialState, action){

  switch (action.type){

    case CHANGE_STATUS_LOAD_ORGANIZATIONS:
      return Object.assign({}, state, { isLoad: action.bool })

    case RECEIVE_ORGANIZATIONS:
      return Object.assign({}, state, { data: action.data })

    default:
      return state

  }

}