import {
  CHANGE_STATUS_LOAD_WORKERS,
  RECEIVE_WORKERS
} from '../action/actionsWorkers';

const initialState = {
  isLoad: false,
  nameRus: 'Сотрудники'
}

export function  workers (state = initialState, action){



  switch (action.type){

    case CHANGE_STATUS_LOAD_WORKERS:
      return Object.assign({}, state, { isLoad: action.bool })

    case RECEIVE_WORKERS:
      return Object.assign({}, state, { data: action.data })

    default:
      return state

  }

}