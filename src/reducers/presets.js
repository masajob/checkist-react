import {
  CHANGE_STATUS_LOAD_PRESETS,
  RECEIVE_PRESETS,
  TOGGLE_PRESET,
  CLEAR_PRESETS
} from '../action/actionsPresets';

const initialState = {
  isLoad: false,
  nameRus: 'Пресеты'
}

export function  presets (state = initialState, action){

  switch (action.type){

    case CHANGE_STATUS_LOAD_PRESETS:
      return Object.assign({}, state, { isLoad: action.bool })

    case RECEIVE_PRESETS:
      return Object.assign({}, state, { data: action.data })

    case TOGGLE_PRESET:
      return Object.assign({}, state, { data: state.data.map((item) => {
          if ( item._id === action.id ) {
            item.selected = true;
          } else {
            item.selected = false;
          }
          return item;
        })
      })

    case CLEAR_PRESETS:
      return Object.assign({}, state, { data: state.data.map((item) => {
          item.selected = false;
          return item;
        })
      })

    default:
      return state

  }

}