import { CHANGE_ADMIN_ROUTE } from '../action/actionAdminRoute'

const initialState = {
  route: '/'
}

export function  adminRoute (state = initialState, action){

  switch (action.type){

    case CHANGE_ADMIN_ROUTE:
      return Object.assign({}, state, { route: action.route })

    default:
      return state

  }

}