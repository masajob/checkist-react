export const CHANGE_STATUS_LOAD_PRESETS = 'CHANGE_STATUS_LOAD_PRESETS';
export const RECEIVE_PRESETS            = 'RECEIVE_PRESETS';
export const TOGGLE_PRESET              = 'TOGGLE_PRESET';
export const CLEAR_PRESETS              = 'CLEAR_PRESETS';

export function aChangeStatusLoadPresets(bool) {
  return {
    type: CHANGE_STATUS_LOAD_PRESETS,
    bool
  }
}

export function aReceivePresets(data) {
  return {
    type: RECEIVE_PRESETS,
    data
  }
}

export function aTogglePreset(id) {
  return {
    type: TOGGLE_PRESET,
    id
  }
}

export function aClearPresets () {
  return {
    type: CLEAR_PRESETS
  }
}