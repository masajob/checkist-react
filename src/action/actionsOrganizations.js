export const CHANGE_STATUS_LOAD_ORGANIZATIONS = 'CHANGE_STATUS_LOAD_ORGANIZATIONS';
export const RECEIVE_ORGANIZATIONS            = 'RECEIVE_ORGANIZATIONS';

export function aChangeStatusLoadOrganizations(bool){
  return {
    type: CHANGE_STATUS_LOAD_ORGANIZATIONS,
    bool
  }
}

export function aReceiveOrganizations(data){
  return {
    type: RECEIVE_ORGANIZATIONS,
    data
  }
}