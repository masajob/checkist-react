export const CHANGE_STATUS_LOAD_SUBMODULES = 'CHANGE_STATUS_LOAD_SUBMODULES';
export const RECEIVE_SUBMODULES            = 'RECEIVE_SUBMODULES';
export const TOGGLE_PARAGRAPH              = 'TOGGLE_PARAGRAPH';
export const SELECT_DEFAULT_PARAGRAPH      = 'SELECT_DEFAULT_PARAGRAPH';
export const SELECT_ALL_PARAGRAPH          = 'SELECT_ALL_PARAGRAPH';
export const CLOSE_SUBMODULES              = 'CLOSE_SUBMODULES';

export function aChangeStatusLoadSubmodules (bool) {
  return {
    type: CHANGE_STATUS_LOAD_SUBMODULES,
    bool
  }
}

export function aReceiveSubmodules (data) {
  return {
    type: RECEIVE_SUBMODULES,
    data
  }
}

export function aToggleParagraph (id) {
  return {
    type: TOGGLE_PARAGRAPH,
    id
  }
}

export function aSelectDefaultParagraph (arrId = []) {
  return {
    type: SELECT_DEFAULT_PARAGRAPH,
    arrId
  }
}

export function aSelectAllParagraph (arrId) {
  return {
    type: SELECT_ALL_PARAGRAPH,
    arrId
  }
}

export function aCloseSubmodules (id) {
  return {
    type: CLOSE_SUBMODULES,
    id
  }
}