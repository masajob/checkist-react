import fetch from 'cross-fetch';
import { aChangeStatusLoadSubmodules, aReceiveSubmodules } from './actionsSubmodules';
import { aChangeStatusLoadModules, aReceiveModules } from './actionsModules';
import { aChangeStatusLoadPresets, aReceivePresets } from './actionsPresets';
import { aChangeStatusLoadWorkers, aReceiveWorkers } from './actionsWorkers';
import { aChangeStatusLoadOrganizations, aReceiveOrganizations } from './actionsOrganizations';

export function aGetData (url) {

  let dChange, dReceive;
  let api = url.split('/')[ url.split('/').length -1 ];

  switch(api) {

    case 'modules':
      dChange = aChangeStatusLoadModules;
      dReceive = aReceiveModules;
      break;

    case 'presets':
      dChange = aChangeStatusLoadPresets;
      dReceive = aReceivePresets;
      break;

    case 'submodules':
      dChange = aChangeStatusLoadSubmodules;
      dReceive = aReceiveSubmodules;
      break;

    case 'workers':
      dChange = aChangeStatusLoadWorkers;
      dReceive = aReceiveWorkers;
      break;

    case 'organizations':
      dChange = aChangeStatusLoadOrganizations;
      dReceive = aReceiveOrganizations;
      break;

    default:
      dChange = null;
      dReceive = null;
  }

  return (dispatch) => {

    dispatch(dChange(true));

    fetch(url)
        .then(response => {
          return response.json()
        })
        .then(json => {
          return dispatch(dReceive(json))
        })

  }
}

export function aCreateData (url, data) {

  let dChange, dReceive;
  let api = url.split('/')[ url.split('/').length -1 ];

  switch(api) {

    case 'modules':
      dChange = aChangeStatusLoadModules;
      dReceive = aReceiveModules;
      break;

    case 'presets':
      dChange = aChangeStatusLoadPresets;
      dReceive = aReceivePresets;
      break;

    case 'submodules':
      dChange = aChangeStatusLoadSubmodules;
      dReceive = aReceiveSubmodules;
      break;

    case 'workers':
      dChange = aChangeStatusLoadWorkers;
      dReceive = aReceiveWorkers;
      break;

    case 'organizations':
      dChange = aChangeStatusLoadOrganizations;
      dReceive = aReceiveOrganizations;
      break;

    default:
      dChange = null;
      dReceive = null;
  }

  return (dispatch) => {

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(json => dispatch(dReceive(json)))

  }
}
