export const CHANGE_STATUS_LOAD_MODULES = 'CHANGE_STATUS_LOAD_MODULES';
export const RECEIVE_MODULES            = 'RECEIVE_MODULES';
export const TOGGLE_MODULE              = 'TOGGLE_MODULE';
export const TOGGLE_MODULES             = 'TOGGLE_MODULES';

export function aChangeStatusLoadModules(bool){
  return {
    type: CHANGE_STATUS_LOAD_MODULES,
    bool
  }
}

export function aReceiveModules(data){
  return {
    type: RECEIVE_MODULES,
    data
  }
}

export function aToggleModule(id){
  return {
    type: TOGGLE_MODULE,
    id
  }
}

export function aToggleModules(arr){
  return {
    type: TOGGLE_MODULES,
    arr
  }
}

