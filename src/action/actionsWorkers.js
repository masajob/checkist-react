export const CHANGE_STATUS_LOAD_WORKERS = 'CHANGE_STATUS_LOAD_MODULES';
export const RECEIVE_WORKERS            = 'RECEIVE_WORKERS';

export function aChangeStatusLoadWorkers(bool){
  return {
    type: CHANGE_STATUS_LOAD_WORKERS,
    bool
  }
}

export function aReceiveWorkers(data){
  return {
    type: RECEIVE_WORKERS,
    data
  }
}