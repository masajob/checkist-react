export const GET_VARIANTS            = 'GET_VARIANTS'
export const SELECT_ALL_VARIANTS     = 'SELECT_ALL_VARIANTS'
export const REMOVE_ALL_VARIANTS     = 'REMOVE_ALL_VARIANTS'
export const SELECT_ONE_VARIANT      = 'SELECT_ONE_VARIANT'
export const REMOVE_HIDDEN_VARIANTS  = 'REMOVE_HIDDEN_VARIANTS'
export const FILTER_VARIANTS         = 'FILTER_VARIANTS'
export const CHANGE_VISIBLE_VARIANTS = 'CHANGE_VISIBLE_VARIANTS'

export function aGetVariants(state){
  return {
    type: GET_VARIANTS,
    state
  }
}

export function aSelectAllVariants () {
  return {
    type: SELECT_ALL_VARIANTS
  }
}

export function aSelectOneVariant (id) {
  return {
    type: SELECT_ONE_VARIANT,
    id
  }
}

export function aRemoveAllVariants () {
  return {
    type: REMOVE_ALL_VARIANTS
  }
}

export function aRemoveHiddenVariants () {
  return {
    type: REMOVE_HIDDEN_VARIANTS
  }
}

export function aFilterVariants (valueInterval) {
  return {
    type: FILTER_VARIANTS,
    valueInterval
  }
}

export function aChangeVisibleVariants (valueInterval) {
  return {
    type: CHANGE_VISIBLE_VARIANTS,
    valueInterval
  }
}