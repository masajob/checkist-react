export const CHANGE_ADMIN_ROUTE = 'CHANGE_ADMIN_ROUTE'

export function aChangeAdminRoute(route){
  return {
    type: CHANGE_ADMIN_ROUTE,
    route
  }
}