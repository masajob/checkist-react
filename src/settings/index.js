export default {
  prefix:        'http://localhost',
  port:          '3000',
  modules:       'api/modules',
  presets:       'api/presets',
  submodules:    'api/submodules',
  workers:       'api/workers',
  organizations: 'api/organizations'
}