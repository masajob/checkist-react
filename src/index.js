import React    from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import createHistory from 'history/createBrowserHistory'
import { Route }     from 'react-router'

import { ConnectedRouter, routerMiddleware} from 'react-router-redux'

import checkistApp from './reducers/index'
import App from './component/App/App'
import CheckList from './component/CheckList/CheckList'
import AdminPanel from './component/AdminPanel'
import getAllData from './getDataFromApi'

const history = createHistory()
const middleware = routerMiddleware(history)

const store = createStore(checkistApp, applyMiddleware(thunk,middleware))
getAllData(store)

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <div>
                <Route exact path="/"  component={App} />
                <Route path="/checklist"  component={CheckList} />
                <Route path="/admin"  component={AdminPanel} />
            </div>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);
