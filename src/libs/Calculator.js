class Calculator {

    constructor(state){
        this._modules = state.modules.data
        this._subModules = state.submodules.data
        this._organization = state.organizations.data
        this._workers = state.workers.data
    }

    getOrganization = () => {
        return this._organization
    }

    getWorkers = () => {
        return this._workers
    }

    getNumberHours = () => {

        let [frontHours,backHours,desHours,arrNameSubModules,arrInternal] = [0,0,0,[],[]]

        this._modules.map((module) => {

            if(module.selected){
                arrInternal = [ ...arrInternal,
                                ...module.internalSubModuleItems]
            }

        })

        this._subModules.map((item) => {

            let elementIs = arrInternal.indexOf(item._id) !== -1

            if(item.selected && elementIs) {
                frontHours += item.frontHours
                backHours += item.backendHours
                desHours += item.designerHours
                arrNameSubModules.push(item.name)
            }

        })

        return {
            frontHours: frontHours,
            backHours: backHours,
            desHours: desHours,
            arrNameSubModules: arrNameSubModules
        }
    }


    getDataConsolidation = () => {

        let organization,workers;
        let arrResult = [];

        organization = this.getOrganization()
        workers = this.getWorkers()

        organization.map((org) => {

            let consOrg = {};
            consOrg.organizationId = org._id
            consOrg.organizationName = org.name
            consOrg.organizationWorks = {
                                            frontend: [],
                                            backend: [],
                                            designer: []
                                        }

            workers.map((worker) => {

                if (worker.orgAffiliation === org._id) {

                    if(worker.position === 1){
                        consOrg.organizationWorks.frontend.push(worker)
                    } else if(worker.position === 2){
                        consOrg.organizationWorks.backend.push(worker)
                    } else if(worker.position === 3){
                        consOrg.organizationWorks.designer.push(worker)
                    }

                }

            })

            arrResult.push(consOrg)

        })

        return arrResult

    }

    getOptions = () => {

        let organization,hours;
        let arrResultOption = [];
        let arrResultSum = [];
        let minSum = 0;
        let maxSum = 0;
        let resultObject = {};

        organization = this.getDataConsolidation()
        hours = this.getNumberHours()

        organization.map((organization) => {

            let frontend,backend,designer;

            frontend = organization.organizationWorks.frontend
            backend = organization.organizationWorks.backend
            designer = organization.organizationWorks.designer

            // Filters
            if( frontend.length === 0 && hours.frontHours > 0) return
            if( backend.length === 0 && hours.backHours > 0) return
            if( designer.length === 0 && hours.desHours > 0) return

            if( frontend.length === 0 && hours.frontHours === 0) frontend = [ { costPerHour: 0 } ]
            if( backend.length === 0 && hours.backHours === 0) backend = [ { costPerHour: 0 } ]
            if( designer.length === 0 && hours.desHours === 0) designer = [ { costPerHour: 0 } ]

            if( frontend.length > 0 && hours.frontHours === 0) frontend = [ { costPerHour: 0 } ]
            if( backend.length > 0 && hours.backHours === 0) backend = [ { costPerHour: 0 } ]
            if( designer.length > 0 && hours.desHours === 0) designer = [ { costPerHour: 0 } ]

            frontend.map((front) => {
                backend.map((back) => {
                    designer.map((des) => {
                        let obj = {}
                        hours.frontHours === 0 ? obj.front = null : obj.front = front
                        hours.backHours === 0 ? obj.back = null : obj.back = back
                        hours.desHours === 0 ? obj.des = null : obj.des = des
                        obj.sum = front.costPerHour*hours.frontHours +
                                  back.costPerHour*hours.backHours +
                                  des.costPerHour*hours.desHours
                        obj.organizationId = organization.organizationId
                        obj.organizationName = organization.organizationName
                        obj.visible = true
                        obj.checked = false
                        obj.id = '_' + Math.random().toString(36).substr(2, 9)
                        arrResultOption.push(obj)
                        arrResultSum.push(obj.sum)
                    })
                })
            })
        })

        minSum = Math.min.apply(null, arrResultSum);
        maxSum = Math.max.apply(null, arrResultSum);

        if( hours.frontHours === 0 && hours.backHours === 0 && hours.desHours === 0){
            resultObject = {
              resultOption: [],
              resultHours: null,
              minSum: null,
              maxSum: null,
              sliderValue: null
            }
        } else {
            resultObject = {
                resultOption: arrResultOption,
                resultHours: hours,
                minSum: minSum,
                maxSum: maxSum,
                sliderValue: [minSum, maxSum]
            }
        }

        return resultObject
    }
}

export default Calculator
